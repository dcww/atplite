package com.stimicode.atplite.Threading.Tasks;


import com.stimicode.atplite.main.Global;
import com.stimicode.atplite.object.ATPQuestion;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Derrick on 6/25/2015.
 */
public class ATPQuestionIterator implements Runnable {

    private static ConcurrentLinkedQueue<ATPQuestion> queue = new ConcurrentLinkedQueue<>();

    @Override
    public void run() {

        for (int x = 0; x < 50; x++) {
            ATPQuestion atpQuestion = queue.poll();
            if (atpQuestion == null) {
                return;
            }
            if (atpQuestion.getTime().getTime() + 30000 < new Date().getTime()) {
                Global.removeATPQuestion(atpQuestion);
                continue;
            }
            queue.add(atpQuestion);
        }
    }

    public static void addQueue(ATPQuestion atpQuestion) { queue.add(atpQuestion); }
}
