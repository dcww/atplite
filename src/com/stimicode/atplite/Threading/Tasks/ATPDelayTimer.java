package com.stimicode.atplite.Threading.Tasks;

import com.stimicode.atplite.main.Global;
import com.stimicode.atplite.object.ATPQuestion;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Derrick on 9/16/2015.
 */
public class ATPDelayTimer implements Runnable {

    public static ConcurrentLinkedQueue<ATPQuestion> queue = new ConcurrentLinkedQueue<>();

    @Override
    public void run() {
        for (int x = 0; x < 50; x++) {
            ATPQuestion atpQuestion = queue.poll();
            if (atpQuestion == null) {
                return;
            }
            try {
                if (atpQuestion.getAccepted().getTime() + TimeUnit.SECONDS.toMillis((long) Global.settingsManager.getInteger("time-delay")) < new Date().getTime()){
                    Global.handleATPRequest(atpQuestion);
                    Global.removeATPQuestion(atpQuestion);
                    continue;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            queue.add(atpQuestion);
        }
    }

    public static void addQueue(ATPQuestion atpQuestion) { queue.add(atpQuestion); }
}