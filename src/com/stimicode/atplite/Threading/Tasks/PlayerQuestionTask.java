package com.stimicode.atplite.Threading.Tasks;


import com.stimicode.atplite.main.Global;
import com.stimicode.atplite.main.MessageManager;
import com.stimicode.atplite.response.QuestionResponseInterface;
import com.stimicode.atplite.util.C;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/19/2015.
 */
public class PlayerQuestionTask extends QuestionBaseTask implements Runnable {

    Player askedPlayer;                                 //Player being questioned
    Player questionPlayer;                              //Player who asked the question
    String question;                                    //Question being asked
    long timeout;                                       //Time before question times out
    QuestionResponseInterface finishedFunction;

    protected String response = new String();           //Response to the Question
    protected Boolean responded = new Boolean(false);   //Whether or not the question was answered

    public PlayerQuestionTask(Player ap, Player qp, String question, long timeout, QuestionResponseInterface finishedFunction) {
        this.askedPlayer = ap;
        this.questionPlayer = qp;
        this.question = question;
        this.timeout = timeout;
        this.finishedFunction = finishedFunction;
    }

    @Override
    public void run() {
        MessageManager.send(askedPlayer, C.Gray + "Question from: " + C.Aqua + questionPlayer.getName());
        MessageManager.send(askedPlayer, C.Gold + C.BOLD + question);
        MessageManager.send(askedPlayer, C.Gray + "Respond by typing " + C.Aqua + "/accept" + C.Gray + " or " + C.Aqua + "/deny");

        try {
            synchronized(this) {
                this.wait(timeout);
            }
        } catch (InterruptedException e) {
            cleanup();
            return;
        }

        if (responded) {
            finishedFunction.processResponse(response);
            cleanup();
            return;
        }

        MessageManager.send(askedPlayer, C.Gray + "You failed to respond to the question from " + questionPlayer.getName() + " in time.");
        MessageManager.send(questionPlayer, C.Gray + askedPlayer.getName() + " failed to answer the question in time.");
        cleanup();
    }

    public Boolean getResponded() {
        synchronized(responded) {
            return responded;
        }
    }

    public void setResponded(Boolean response) {
        synchronized(this.responded) {
            this.responded = response;
        }
    }

    public String getResponse() {
        synchronized(response) {
            return response;
        }
    }

    public void setResponse(String response) {
        synchronized(this.response) {
            setResponded(true);
            this.response = response;
        }
    }

    /* When this task finishes, remove itself from the hashtable. */
    private void cleanup() {
        Global.removeQuestion(askedPlayer.getName());
    }
}
