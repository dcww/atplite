package com.stimicode.atplite.util;

import org.bukkit.Location;

/**
 * Created by Derrick on 9/17/2015.
 */
public class BlockUtil {

    public static boolean getBlockCompare(Location from, Location to) {
        if (from.getBlockX() != to.getBlockX() || from.getBlockY() != to.getBlockY() || from.getBlockZ() != to.getBlockZ()) {
            return true;
        }
        return false;
    }
}
