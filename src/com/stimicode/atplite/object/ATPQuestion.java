package com.stimicode.atplite.object;

import org.bukkit.entity.Player;

import java.util.Date;

/**
 * Created by Derrick on 9/2/2015.
 */
public class ATPQuestion {

    public Player target;
    public Player requester;

    public Date accepted;
    public Date time;

    public ATPQuestion(Player _target, Player _requester) {
        target = _target;
        requester = _requester;
        time = new Date();
    }

    public Date getTime() {
        return time;
    }

    public Date getAccepted() { return accepted; }

    public void setAccepted() {
        accepted = new Date();
    }
}
