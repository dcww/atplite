package com.stimicode.atplite.command;

import com.stimicode.atplite.main.Global;
import com.stimicode.atplite.main.MessageManager;
import com.stimicode.atplite.util.C;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/1/2015.
 */
public class ATPCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player)sender;

            if (!(player.hasPermission("atplite.teleport"))) {
                MessageManager.sendError(player, "You do not have permission to run this command.");
                return false;
            }

            if (args.length < 1) {
                MessageManager.sendError(player, "Please enter a player you would like to teleport too.");
                return false;
            }

            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                MessageManager.sendError(player, "Please enter a valid player name.");
                return false;
            }

            if (Global.hasATPRequest(target)) {
                MessageManager.sendError(player, "The current player already has a pending teleport request. Try again in a bit.");
                return false;
            }

            Global.addATPQuestion(target, player);
            MessageManager.send(target, C.Green + player.getDisplayName() + C.Gold + " has requested to teleport to you. Type /Accept or /Deny.");
            MessageManager.send(player, C.Gold + "A teleport request has been sent to " + C.Green + target.getDisplayName() + C.Gold + ".");
            return true;
        }
        return false;
    }
}
