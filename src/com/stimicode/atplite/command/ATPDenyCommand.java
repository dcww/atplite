package com.stimicode.atplite.command;

import com.stimicode.atplite.main.Global;
import com.stimicode.atplite.main.MessageManager;
import com.stimicode.atplite.object.ATPQuestion;
import com.stimicode.atplite.util.C;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/14/2015.
 */
public class ATPDenyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (!(player.hasPermission("atplite.deny"))) {
                MessageManager.sendError(player, "You do not have permission to run this command.");
                return false;
            }

            if (Global.hasATPRequest(player)) {
                ATPQuestion atpQuestion = Global.getATPQuestion(player);
                Global.removeATPQuestion(atpQuestion);
                MessageManager.send(atpQuestion.requester, C.Green + atpQuestion.target.getDisplayName() + C.Gold + " has denied your teleport request.");
                MessageManager.send(atpQuestion.target, C.Gold + "Teleport request has been denied.");
                return true;
            } else {
                MessageManager.sendError(player, "No request to respond to.");
            }
        }
        return false;
    }
}
