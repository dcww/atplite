package com.stimicode.atplite.command;

import com.stimicode.atplite.Threading.Tasks.ATPDelayTimer;
import com.stimicode.atplite.main.Global;
import com.stimicode.atplite.main.MessageManager;
import com.stimicode.atplite.object.ATPQuestion;
import com.stimicode.atplite.util.C;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/14/2015.
 */
public class ATPAcceptCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (!(player.hasPermission("atplite.accept"))) {
                MessageManager.sendError(player, "You do not have permission to run this command.");
                return false;
            }

            if (Global.hasATPRequest(player)) {
                ATPQuestion atpQuestion = Global.getATPQuestion(player);
                try {
                    if (Global.settingsManager.getBoolean("anti-tp")) {
                        MessageManager.send(atpQuestion.requester, C.Gold + "Teleport request has been accepted. Teleporting to " + C.Green + atpQuestion.target.getDisplayName() + C.Gold + " in " + Global.settingsManager.getInteger("time-delay") + " seconds.");
                        MessageManager.send(atpQuestion.target, C.Gold + "Teleport request has been accepted. " + C.Green + atpQuestion.requester.getDisplayName() + C.Gold + " will teleport to you in " + Global.settingsManager.getInteger("time-delay") + " seconds.");
                        atpQuestion.setAccepted();
                        ATPDelayTimer.addQueue(atpQuestion);
                    } else {
                        MessageManager.send(atpQuestion.requester, C.Gold + "Teleport request has been accepted. Teleported to " + C.Green + atpQuestion.target.getDisplayName() + C.Gold + ".");
                        MessageManager.send(atpQuestion.target, C.Gold + "Teleport request has been accepted. " + C.Green + atpQuestion.requester.getDisplayName() + C.Gold + " has been teleported to you.");
                        Global.handleATPRequest(atpQuestion);
                        Global.removeATPQuestionTarget(player);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            } else {
                MessageManager.sendError(player, "No request to respond to.");
            }
        }
        return false;
    }
}