package com.stimicode.atplite.listener;

import com.stimicode.atplite.Threading.Tasks.ATPDelayTimer;
import com.stimicode.atplite.main.Global;
import com.stimicode.atplite.main.MessageManager;
import com.stimicode.atplite.object.ATPQuestion;
import com.stimicode.atplite.util.BlockUtil;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Derrick on 9/1/2015.
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerDisconnect(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Global.removeATPQuestion(player);
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) throws Exception {
        if (!(Global.settingsManager.getBoolean("anti-tp"))) {
            return;
        }

        if (BlockUtil.getBlockCompare(event.getFrom(), event.getTo())) {
            for (ATPQuestion atpQuestion : Global.atpQuestions.values()) {
                if (atpQuestion.requester.getUniqueId() == event.getPlayer().getUniqueId()) {
                    if (ATPDelayTimer.queue.contains(atpQuestion)) {
                        ATPDelayTimer.queue.remove(atpQuestion);
                        Global.removeATPQuestion(atpQuestion);
                        MessageManager.sendError(event.getPlayer(), "Teleportation has been canceled because you moved.");
                    }
                }
            }
        }
    }
}
