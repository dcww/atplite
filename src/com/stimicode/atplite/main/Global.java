package com.stimicode.atplite.main;

import com.stimicode.atplite.Threading.TaskMaster;
import com.stimicode.atplite.Threading.Tasks.ATPQuestionIterator;
import com.stimicode.atplite.Threading.Tasks.PlayerQuestionTask;
import com.stimicode.atplite.Threading.Tasks.QuestionBaseTask;
import com.stimicode.atplite.object.ATPQuestion;
import com.stimicode.atplite.response.QuestionResponseInterface;
import com.stimicode.atplite.util.C;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Derrick on 9/1/2015.
 */
public class Global {

    public static SettingsManager settingsManager;
    public static HashMap<String, ATPQuestion> atpQuestions = new HashMap<>();
    private static Map<String, QuestionBaseTask> questions = new ConcurrentHashMap<>();

    public static void addATPQuestion(Player target, Player player) {
        ATPQuestion temp = new ATPQuestion(target, player);
        atpQuestions.put(player.getUniqueId().toString(), temp);
        ATPQuestionIterator.addQueue(temp);
    }

    public static void removeATPQuestion(ATPQuestion atpQuestion) {
        for (String string : atpQuestions.keySet()) {
            if (atpQuestions.get(string) == atpQuestion) {
                atpQuestions.remove(string);
            }
        }
    }

    public static void removeATPQuestionTarget(Player player) {
        for (ATPQuestion atpQuestion : atpQuestions.values()) {
            if (atpQuestion.target.getUniqueId().equals(player.getUniqueId())) {
                atpQuestions.remove(atpQuestion.requester.getUniqueId().toString());
            }
        }
    }

    public static void removeATPQuestion(Player player) {
        for (ATPQuestion atpQuestion : atpQuestions.values()) {
            if (atpQuestion.target.getUniqueId().equals(player.getUniqueId()) || atpQuestion.requester.getUniqueId().equals(player.getUniqueId())) {
                atpQuestions.remove(atpQuestion.requester.getUniqueId().toString());
            }
        }
    }

    public static boolean containsATPQuestion(Player player) {
        for (ATPQuestion atpQuestion : atpQuestions.values()) {
            if (atpQuestion.target.getUniqueId().equals(player.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasATPRequest(Player player) {
        for (ATPQuestion atpQuestion : atpQuestions.values()) {
            if (atpQuestion.target.getUniqueId().equals(player.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    public static ATPQuestion getATPQuestion(Player target) {
        for (ATPQuestion atpQuestion : atpQuestions.values()) {
            if (atpQuestion.target.getUniqueId().equals(target.getUniqueId())) {
                return atpQuestion;
            }
        }
        return null;
    }

    public static void handleATPRequest(ATPQuestion atpQuestion) {
        atpQuestion.requester.teleport(atpQuestion.target);
    }

    public static void questionPlayer(Player fromPlayer, Player toPlayer, String question, long timeout, QuestionResponseInterface finishedFunction) throws Exception {
        PlayerQuestionTask task = (PlayerQuestionTask) questions.get(toPlayer.getName());
        if (task != null) {
			/* Player already has a question pending. Lets deny this question until it times out
			 * this will allow questions to come in on a pseduo 'first come first serve' and
			 * prevents question spamming.
			 */
            throw new Exception("Player already has a question pending, wait 30 seconds and try again.");
        }

        task = new PlayerQuestionTask(toPlayer, fromPlayer, question, timeout, finishedFunction);
        questions.put(toPlayer.getName(), task);
        TaskMaster.asyncTask("", task, 0);
    }

    public static QuestionBaseTask getQuestionTask(String string) {
        return questions.get(string);
    }

    public static void removeQuestion(String name) {
        questions.remove(name);
    }
}
