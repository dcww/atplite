package com.stimicode.atplite.main;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * Created by Derrick on 6/17/2015.
 */
public class SettingsManager {

    public JavaPlugin plugin;

    public FileConfiguration config; /* Main Configuration */

    public void init (JavaPlugin javaplugin) {
        plugin = javaplugin;
        config = javaplugin.getConfig();

        if (!javaplugin.getDataFolder().exists()) {
            javaplugin.getDataFolder().mkdir();
        }
    }

    public FileConfiguration loadConfigurationFile(String filepath) throws IOException, InvalidConfigurationException {
        File file = new File(plugin.getDataFolder().getPath()+"/"+filepath);
        if (file.exists()) {
            Log.info("Loading Configuration file:" + filepath);

            YamlConfiguration cfg = new YamlConfiguration();
            cfg.load(file);
            return cfg;
        } else {
            Log.warning("Configuration file:" + filepath + " was missing. Attempting to create.");
            file.createNewFile();
            loadConfigurationFile(filepath);
        }
        return null;
    }

    public String getString(String path) throws Exception {
        return getString(plugin.getConfig(), path);
    }

    public Integer getInteger(String path) throws Exception {
        return getInteger(plugin.getConfig(), path);
    }

    public double getDouble(String path) throws Exception {
        return getDouble(plugin.getConfig(), path);
    }

    public boolean getBoolean(String path) throws Exception {
        return getBoolean(plugin.getConfig(), path);
    }

    public Integer getInteger(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration integer "+path);
        }

        return cfg.getInt(path);
    }

    public String getString(FileConfiguration cfg, String path) throws Exception {
        String data = cfg.getString(path);
        if (data == null) {
            throw new Exception("Could not get configuration string "+path);
        }
        return data;
    }

    public double getDouble(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration double "+path);
        }
        return cfg.getDouble(path);
    }

    public boolean getBoolean(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration boolean "+path);
        }
        return cfg.getBoolean(path);
    }
}
