package com.stimicode.atplite.main;

import com.stimicode.atplite.util.C;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Derrick on 6/18/2015.
 */
public class MessageManager {

    public static void sendError(Object sender, String line) {
        send(sender, C.Red + line);
    }

    public static void send(Object sender, String line) {
        if ((sender instanceof Player)) {
            ((Player) sender).sendMessage(line);
        } else if (sender instanceof CommandSender) {
            ((CommandSender) sender).sendMessage(line);
        }
    }
}
