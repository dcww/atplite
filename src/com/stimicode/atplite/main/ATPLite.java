package com.stimicode.atplite.main;

import com.stimicode.atplite.Threading.TaskMaster;
import com.stimicode.atplite.Threading.Tasks.ATPQuestionIterator;
import com.stimicode.atplite.Threading.Tasks.ATPDelayTimer;
import com.stimicode.atplite.command.ATPAcceptCommand;
import com.stimicode.atplite.command.ATPCommand;
import com.stimicode.atplite.command.ATPDenyCommand;
import com.stimicode.atplite.listener.PlayerListener;
import com.stimicode.atplite.util.BukkitUtility;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Derrick on 9/1/2015.
 */
public class ATPLite extends JavaPlugin {

    @Override
    public void onEnable() {
        initialize();
        TaskMaster.asyncTimer("GameStartTimer", new ATPQuestionIterator(), 20);
        TaskMaster.syncTimer("GameStartTimer", new ATPDelayTimer(), 10);
        this.saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        //TODO On Disable Code
    }

    public void initialize() {
        Log.init(this);
        initCommands();
        listeners();
        BukkitUtility.initialize(this);
        Global.settingsManager = new SettingsManager();
        Global.settingsManager.init(this);
    }

    private void listeners() {
        final PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(new PlayerListener(), this);
    }

    private void initCommands() {
        getCommand("atp").setExecutor(new ATPCommand());
        getCommand("accept").setExecutor(new ATPAcceptCommand());
        getCommand("deny").setExecutor(new ATPDenyCommand());
    }
}
