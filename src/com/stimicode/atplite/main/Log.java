package com.stimicode.atplite.main;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Derrick on 6/18/2015.
 */
public class Log {
    public static JavaPlugin plugin;
    private static Logger cleanupLogger;

    public static void init(JavaPlugin plugin) {
        Log.plugin = plugin;

        cleanupLogger = Logger.getLogger("cleanUp");
        FileHandler fh;

        try {
            fh = new FileHandler("cleanUp.log");
            cleanupLogger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void info(String message) {
        plugin.getLogger().info(message);
    }

    public static void debug(String message) {
        plugin.getLogger().info("[DEBUG] "+message);
    }

    public static void warning(String message) {
        if (message == null) {
            try {
                throw new Exception("Null warning message!");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        //if (Global.warningsEnabled) {
        plugin.getLogger().info("[WARNING] "+message);
        //}
    }

    public static void error(String message) {
        plugin.getLogger().severe(message);
    }

    public static void cleanupLog(String message) {
        info(message);
        cleanupLogger.info(message);
    }
}
