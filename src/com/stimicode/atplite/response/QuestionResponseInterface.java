package com.stimicode.atplite.response;

/**
 * Created by Derrick on 9/19/2015.
 */
public interface QuestionResponseInterface {
    void processResponse(String param);
}
