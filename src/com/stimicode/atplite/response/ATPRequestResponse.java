package com.stimicode.atplite.response;

import com.stimicode.atplite.main.MessageManager;
import com.stimicode.atplite.object.ATPQuestion;
import com.stimicode.atplite.util.C;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/20/2015.
 */
public class ATPRequestResponse implements QuestionResponseInterface {

    ATPQuestion atpQuestion;
    Player target;
    Player sender;

    @Override
    public void processResponse(String param) {
        if (param.equalsIgnoreCase("accept")) {
            MessageManager.send(sender, C.Gray + target.getName() + " has accepted your teleport request.");
        } else {
            MessageManager.send(sender, C.Gray + target.getName() + " denied your teleport request.");
        }
    }
}
